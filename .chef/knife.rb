# See https://docs.chef.io/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "ams16"
client_key               "#{current_dir}/ams16.pem"
validation_client_name   "ams-chef-validator"
validation_key           "#{current_dir}/ams-chef-validator.pem"
chef_server_url          "https://api.opscode.com/organizations/ams-chef"
cookbook_path            ["#{current_dir}/../cookbooks"]
